//
//  CMSharedManager.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/10/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMSharedManager.h"

@implementation CMSharedManager


+ (instancetype)sharedManager {
    static CMSharedManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setDefaultValues];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        self.themeNumber = [[defaults objectForKey:@"THEME_NUMBER"] intValue];
        self.areAdsOff = [[defaults objectForKey:@"ARE_ADS_OFF"] boolValue];
    }
    return self;
}

+ (UIButton *) makeBackButton{
    CMSharedManager *sm = [CMSharedManager sharedManager];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(sm.SIDE_GAP, sm.TOP_GAP_LABEL, sm.AUXILIARY_WIDTH, sm.AUXILIARY_HEIGHT)];
    [backButton setTitle: @"←" forState:UIControlStateNormal];
    [backButton setTitleColor:sm.menuButtonTextColorNormal forState:UIControlStateNormal];
    [backButton setTitleColor:sm.menuButtonTextColorHighlighted forState:UIControlStateHighlighted];
    
    backButton.backgroundColor = sm.menuButtonBackgroundColor;
    backButton.layer.borderColor = (sm.menuButtonTextColorNormal).CGColor;
    backButton.layer.cornerRadius = sm.CORNER_RADIUS;
    [backButton.titleLabel setFont:[UIFont systemFontOfSize:sm.MENU_FONT_SIZE]];
    backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    backButton.clipsToBounds = YES;
    [backButton setExclusiveTouch:YES];
    SEL selector = NSSelectorFromString(@"clickBackButton:");
    if(selector)
        [backButton addTarget:nil action:selector forControlEvents:UIControlEventTouchUpInside];
    return backButton;
}

+ (UIButton *) makeMenuButtonAtLocation:(double)location AndView:(UIView *)view AndSelector:(SEL)selector AndTitle:(NSString *)title{
    CMSharedManager *sm = [CMSharedManager sharedManager];
    return [self makeMenuButtonWithX:view.frame.size.width/2-sm.MENU_WIDTH/2 AndY:sm.TOP_GAP_INFO+(sm.MENU_HEIGHT+sm.BUTTON_GAP)*(location+1) AndSelector:selector AndTitle:title];
}

+ (UIButton *) makeMenuButtonWithX:(double) x AndY:(double) y AndSelector:(SEL)selector AndTitle:(NSString *)title{
    CMSharedManager *sm = [CMSharedManager sharedManager];
    UIButton *menuButton = [[UIButton alloc] initWithFrame: CGRectMake(x, y, sm.MENU_WIDTH, sm.MENU_HEIGHT)];
    [menuButton setTitleColor:sm.menuButtonTextColorNormal forState:UIControlStateNormal];
    [menuButton setTitleColor:sm.menuButtonTextColorHighlighted forState:UIControlStateHighlighted];
    menuButton.backgroundColor = sm.menuButtonBackgroundColor;
    menuButton.layer.cornerRadius = sm.CORNER_RADIUS;
    menuButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [menuButton.titleLabel setFont:[UIFont systemFontOfSize:sm.MENU_FONT_SIZE]];
    [menuButton setTitle:title forState:UIControlStateNormal];
    menuButton.clipsToBounds = YES;
    [menuButton setExclusiveTouch:YES];
    if(selector)
        [menuButton addTarget:nil action:selector forControlEvents:UIControlEventTouchUpInside];
    return menuButton;
}

+ (UILabel *) makeHeadingLabelWithView:(UIView *)view WithText:(NSString *)text {
    CMSharedManager *sm = [CMSharedManager sharedManager];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width/2-sm.HEADING_WIDTH/2, sm.TOP_GAP_INFO, sm.HEADING_WIDTH, sm.HEADING_HEIGHT)];
    [label setTextColor:sm.headingTextColor];
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont boldSystemFontOfSize:sm.HEADING_FONT_SIZE]];
    return label;
}


+(NSString *) getDifficultyName:(int) difficulty {
    switch (difficulty) {
        case DIFFICULTY_EASY:
            return @"Easy";
        case DIFFICULTY_MEDIUM:
            return @"Medium";
        case DIFFICULTY_HARD:
            return @"Hard";
        case DIFFICULTY_VERY_HARD:
            return @"Very Hard";
        default:
            return @"None";
    }
}

+(NSString *) getDifficultyNameWithUnderscore:(int) difficulty {
        return [[self getDifficultyName:difficulty] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
}

+(void) shareToSocialMedia:(UIViewController *) vc {
    [CMSharedManager shareToSocialMedia:vc WithScore:-1];
}


+(void) shareToSocialMedia:(UIViewController *) vc WithScore:(int) score {
    NSString *text;
    if(score < 0) {
        text = @"I'm scoring tons of points at ChroMATCH, an addictive puzzle game where you match colors to score big! #ChromatchFTW";
    }
    else {
        text = [NSString stringWithFormat:@"I scored %i points at ChroMATCH, an addictive game where you match colors to score big! #ChromatchFTW", score];
    }
    NSURL *url = [NSURL URLWithString:@"http://appstore.com/chromatch"];
    //UIImage *image = [UIImage imageNamed:@"roadfire-icon-square-200"];
    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[text, url]
     applicationActivities:nil];
    
    controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    
    [controller setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if(score >= 0) {
            [vc.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    // Present the controller
    [vc presentViewController:controller animated:YES completion:nil];
}

-(void) setDefaultValues {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DefaultValues" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    _BLOCK_MAX = [[dict objectForKey:@"BLOCK_MAX"] intValue];
    _BLOCK_MIN = [[dict objectForKey:@"BLOCK_MIN"] intValue];
    
    _TOP_GAP_BOARD = [[dict objectForKey:@"TOP_GAP_BOARD"] doubleValue]; //112 with iAds
    _TOP_GAP_LABEL = [[dict objectForKey:@"TOP_GAP_LABEL"] doubleValue];
    _TOP_GAP_INFO = [[dict objectForKey:@"TOP_GAP_INFO"] doubleValue];   //45 with iAds
    _TOP_GAP_HIGH_SCORES = [[dict objectForKey:@"TOP_GAP_HIGH_SCORES"] doubleValue];   //45 with iAds
    _SIDE_GAP = [[dict objectForKey:@"SIDE_GAP"] doubleValue];
    _BUTTON_GAP = [[dict objectForKey:@"BUTTON_GAP"] doubleValue];
    _DESCRIPTION_GAP_FACTOR = [[dict objectForKey:@"DESCRIPTION_GAP_FACTOR"] doubleValue];

    
    _BLOCK_BORDER_WIDTH_NORMAL = [[dict objectForKey:@"BLOCK_BORDER_WIDTH_NORMAL"] doubleValue];
    _BLOCK_BORDER_WIDTH_HIGHLIGHTED = [[dict objectForKey:@"BLOCK_BORDER_WIDTH_HIGHLIGHTED"] doubleValue];
    _BUTTON_BORDER_WIDTH = [[dict objectForKey:@"BUTTON_BORDER_WIDTH"] doubleValue];
    
    _MENU_WIDTH = [[dict objectForKey:@"MENU_WIDTH"] doubleValue];
    _MENU_HEIGHT = [[dict objectForKey:@"MENU_HEIGHT"] doubleValue];
    
    _TITLE_WIDTH = [[dict objectForKey:@"TITLE_WIDTH"] doubleValue];
    _TITLE_HEIGHT = [[dict objectForKey:@"TITLE_HEIGHT"] doubleValue];
    
    _HEADING_WIDTH = [[dict objectForKey:@"HEADING_WIDTH"] doubleValue];
    _HEADING_HEIGHT = [[dict objectForKey:@"HEADING_HEIGHT"] doubleValue];

    _CYCLE_BUTTON_WIDTH = [[dict objectForKey:@"CYCLE_BUTTON_WIDTH"] doubleValue];
    _CYCLE_BUTTON_HEIGHT = [[dict objectForKey:@"CYCLE_BUTTON_HEIGHT"] doubleValue];
    
    _TITLE_FONT_SIZE = [[dict objectForKey:@"TITLE_FONT_SIZE"] doubleValue];
    _HEADING_FONT_SIZE = [[dict objectForKey:@"HEADING_FONT_SIZE"] doubleValue];
    _MENU_FONT_SIZE = [[dict objectForKey:@"MENU_FONT_SIZE"] doubleValue];
    _LABEL_FONT_SIZE = [[dict objectForKey:@"LABEL_FONT_SIZE"] doubleValue];
    _CYCLE_BUTTON_FONT_SIZE = [[dict objectForKey:@"CYCLE_BUTTON_FONT_SIZE"] doubleValue];
    _DESCRIPTION_FONT_SIZE = [[dict objectForKey:@"DESCRIPTION_FONT_SIZE"] doubleValue];

    
    _AUXILIARY_WIDTH = [[dict objectForKey:@"AUXILIARY_WIDTH"] doubleValue];
    _AUXILIARY_HEIGHT = [[dict objectForKey:@"AUXILIARY_HEIGHT"] doubleValue];
    
    _LABEL_WIDTH = [[dict objectForKey:@"LABEL_WIDTH"] doubleValue];
    _LABEL_HEIGHT = [[dict objectForKey:@"LABEL_HEIGHT"] doubleValue];
    
    _STREAK_COUNT_MAX = [[dict objectForKey:@"STREAK_COUNT_MAX"] doubleValue];
    _STREAK_COUNT_MULTIPLIER = [[dict objectForKey:@"STREAK_COUNT_MULTIPLIER"] doubleValue];
    
    _TIMER_START = [[dict objectForKey:@"TIMER_START"] intValue];
    _TIMER_WARNING = [[dict objectForKey:@"TIMER_WARNING"] intValue];
    _TIMER_TICK_INCREMENT = [[dict objectForKey:@"TIMER_TICK_INCREMENT"] doubleValue];
    
    _DISPLAY_TIME_WRONG = [[dict objectForKey:@"DISPLAY_TIME_WRONG"] doubleValue];
    _DISPLAY_TIME_RESET = [[dict objectForKey:@"DISPLAY_TIME_RESET"] doubleValue];
    _DISPLAY_TIME_CORRECT = [[dict objectForKey:@"DISPLAY_TIME_CORRECT"] doubleValue];
    
    _IMAGE_NAME_IN_PROGRESS = [dict objectForKey:@"IMAGE_NAME_IN_PROGRESS"];
    _IMAGE_NAME_CORRECT = [dict objectForKey:@"IMAGE_NAME_CORRECT"];
    _IMAGE_NAME_WRONG = [dict objectForKey:@"IMAGE_NAME_WRONG"];
    
    _IMAGE_NAME_BACKGROUND_MENU = [dict objectForKey:@"IMAGE_NAME_BACKGROUND_MENU"];
    _IMAGE_NAME_BACKGROUND_GAME = [dict objectForKey:@"IMAGE_NAME_BACKGROUND_GAME"];
    
    _APP_TITLE = [dict objectForKey:@"APP_TITLE"];
    _INSTRUCTIONS_TEXT = [dict objectForKey:@"INSTRUCTIONS_TEXT"];
    _DEBUG_CAN_DISPLAY_ADS = [[dict objectForKey:@"DEBUG_CAN_DISPLAY_ADS"] boolValue];
    
    _ACHIEVEMENT_POINTS_VALUES = [dict objectForKey:@"ACHIEVEMENT_POINTS_VALUES"];
    _ACHIEVEMENT_ROUNDS_VALUES = [dict objectForKey:@"ACHIEVEMENT_ROUNDS_VALUES"];


}

-(void) setThemeNumber:(int) themeNumber {
    _themeNumber = themeNumber;
    NSString *theme = [NSString stringWithFormat:@"Theme%i",themeNumber];

    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:theme ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    _CORNER_RADIUS = [[dict objectForKey:@"CORNER_RADIUS"] doubleValue];
    
    
    NSArray *c = [dict objectForKey:@"MENU_BUTTON_TEXT_COLOR_NORMAL"];
    _menuButtonTextColorNormal = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"MENU_BUTTON_TEXT_COLOR_HIGHLIGHTED"];
    _menuButtonTextColorHighlighted = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"MENU_BUTTON_BACKGROUND_COLOR"];
    _menuButtonBackgroundColor = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"HEADING_TEXT_COLOR"];
    _headingTextColor = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"BLOCK_BUTTON_BORDER_COLOR_NORMAL"];
    _blockButtonBorderColorNormal = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"BLOCK_BUTTON_BORDER_COLOR_HIGHLIGHTED"];
    _blockButtonBorderColorHighlighted = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"BLOCK_BUTTON_BORDER_COLOR_HIGHLIGHTED_ALTERNATIVE"];
    _blockButtonBorderColorHighlightedAlternative = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"INFO_TEXT_COLOR"];
    _infoTextColor = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"LABEL_TEXT_COLOR"];
    _labelTextColor = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
    
    c = [dict objectForKey:@"WARNING_LABEL_TEXT_COLOR"];
    _warningLabelTextColor = [UIColor colorWithRed:[c[0] doubleValue] green:[c[1] doubleValue] blue:[c[2] doubleValue] alpha:[c[3] doubleValue]];
}

@end
