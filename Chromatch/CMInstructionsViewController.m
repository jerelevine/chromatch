//
//  CMInstructionsViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/20/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMInstructionsViewController.h"

@interface CMInstructionsViewController ()

@end

@implementation CMInstructionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setExclusiveTouch:YES];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    self.sm = [CMSharedManager sharedManager];
    
	// Do any additional setup after loading the view.
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UIButton *backButton = [CMSharedManager makeBackButton];
    [backButton addTarget:self action:@selector(clickBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UILabel *headingLabel = [CMSharedManager makeHeadingLabelWithView:self.view WithText:@"Instructions"];
    [self.view addSubview:headingLabel];

    [self.view addSubview:headingLabel];
    
    UILabel *instructionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-self.sm.HEADING_WIDTH/2, self.sm.TOP_GAP_INFO*2, self.sm.HEADING_WIDTH, 370-(self.sm.TOP_GAP_INFO))];
    [instructionsLabel setTextColor:self.sm.infoTextColor];
    instructionsLabel.text = @"All the colors on the board are (slightly) different except two colors are the exact same.\n\nSelect the two identical colors to proceed to the next puzzle.\n\nSolve puzzles to gain points. Incorrectly guess and lose a point.\n\nCan't solve a puzzle? Hit the \"swap\" button to get a new puzzle.\n\nGet bonus points for solving multiple puzzles in a row without guessing incorrectly.";
    instructionsLabel.textAlignment = NSTextAlignmentLeft;
    [instructionsLabel setFont:[UIFont boldSystemFontOfSize:self.sm.DESCRIPTION_FONT_SIZE]];
    [instructionsLabel setBackgroundColor:[UIColor clearColor]];
    instructionsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    instructionsLabel.numberOfLines = 0;
    [self.view addSubview:instructionsLabel];
    
    
    
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
