//
//  CMGame.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMGame.h"

@implementation CMGame

-(instancetype) init {
    self = [super init];
    return self;
}

-(instancetype) initWithDifficulty:(int)difficulty
{
    self = [super init];
    if(self) {
        _gameStatus = STATE_IN_PROGRESS;
        self.difficulty = difficulty;
        self.width = difficulty;
        self.height = difficulty;
        
        
        NSMutableSet *colorSet = [[NSMutableSet alloc] init];
        NSMutableSet *labelSet = [[NSMutableSet alloc] init];
        double ratio = (ceil(pow(self.width*self.height-1, 1.0f/3.0f)));
        //subtract a delta to make ratio 2 for a 3x3 board
        
        while(colorSet.count < ((self.width * self.height) - 1)) {
            double red = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            double green = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            double blue = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
            [colorSet addObject:color];
        }
        
        
        /*
        double x = (self.width * self.height) - 1;
        double athird = 1.0f/3.0f;
        double left = pow((-(3*sqrt(3)) * sqrt(27*pow(x,2)+(4*x)) + (27*x) + 2), athird) / pow(2, athird);
        NSLog(@"Left: %f", left);

        double ratio = ceil(left/3 + pow(left, -1)/3 + athird-0.0001);
        NSLog(@"Ratio: %f", ratio);
        
        
        while(colorSet.count < ((self.width * self.height) - 1)) {
            double red = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            double green = (arc4random_uniform((int32_t)ratio-1)/(ratio - 2));
            double blue = (arc4random_uniform((int32_t)ratio)/(ratio - 1));
            UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
            [colorSet addObject:color];
        }*/


        while(labelSet.count < ((self.width * self.height) - 1)) {
            if((self.width * self.height)>26*26)
                [NSException raise:@"Invalid Block amount value" format:@"Block amount of %i is invalid", (int) (self.width * self.height)];
            int num1 = (int)(arc4random_uniform(26))+65;
            int num2 = (int)(arc4random_uniform(26))+65;
            NSString *label;
            if(((self.width * self.height) - 1) <= 26)
                label = [NSString stringWithFormat:@"%c", num1];
            else
                label = [NSString stringWithFormat:@"%c%c", num1, num2];
            [labelSet addObject:label];
        }
        
        
        NSMutableArray *colorArray = [NSMutableArray arrayWithArray:[colorSet allObjects]];
        
        NSMutableArray *labelArray = [NSMutableArray arrayWithArray:[labelSet allObjects]];
        NSMutableArray *blockArray = [[NSMutableArray alloc] init];
        for(int i = 0; i < colorArray.count; i++) {
            [blockArray addObject:[[CMBlock alloc] initWithColor:colorArray[i] AndLabel:labelArray[i]]];
        }
        int repeatIndex = arc4random_uniform((int32_t)blockArray.count);
        CMBlock *repeat = [blockArray[repeatIndex] copy];
        int insertIndex = arc4random_uniform((int32_t)blockArray.count);
        [blockArray insertObject:repeat atIndex:insertIndex];

        
        self.board = [NSMutableArray arrayWithCapacity:self.width];
        for(int i = 0; i < self.width; i++) {
            self.board[i] = [NSMutableArray arrayWithCapacity:self.height];
            for(int j = 0; j < self.height; j++) {
                int loc = arc4random_uniform((int32_t)blockArray.count);
                CMBlock *block = blockArray[loc];
                [blockArray removeObjectAtIndex:loc];
                self.board[i][j] = block;
            }
        }
        
        [colorSet removeAllObjects];
        [labelSet removeAllObjects];
        [colorArray removeAllObjects];
        [labelArray removeAllObjects];
        [blockArray removeAllObjects];
    }
    return self;
}

-(BOOL) isMatchOfThisBlock:(CMBlock *)thisBlock andThatBlock:(CMBlock *)thatBlock
{
    return [thisBlock.color isEqual:thatBlock.color];
}


@end

