//
//  CMGame.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMBlock.h"

@interface CMGame : NSObject

@property (strong, nonatomic) NSMutableArray *board;
@property (nonatomic) NSInteger height;
@property (nonatomic) NSInteger width;
@property (nonatomic, readonly) NSInteger gameStatus;
@property (strong, nonatomic) NSMutableArray *matchingBlocks;
@property (nonatomic) int difficulty;


-(instancetype) initWithDifficulty:(int)difficulty;
-(BOOL) isMatchOfThisBlock:(CMBlock *) thisBlock andThatBlock:(CMBlock *)thatBlock;

typedef enum
{
    STATE_NIL,
    STATE_IN_PROGRESS,
    STATE_CORRECT,
    STATE_RESET,
    STATE_WRONG,
    STATE_OVER,
    STATE_QUIT,
    STATE_PRE_START,
} GAME_STATE;
@end

