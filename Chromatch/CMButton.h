//
//  CMButton.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMBlock.h"
#import "CMSharedManager.h"

@interface CMButton : UIButton

@property (nonatomic) CGPoint point;
@property (weak, nonatomic) CMBlock * block;
@property (nonatomic) BOOL chosen;
@property (nonatomic) BOOL isLabelDisplayed;
@property (strong, nonatomic) CMSharedManager* sm;

- (instancetype) initWithLocation:(CGPoint)point andBlock:(CMBlock *) block andSize:(int)blockSize AndIsLabelDisplayed:(BOOL)isLabelDisplayed;
- (void) showMatching;

@end
