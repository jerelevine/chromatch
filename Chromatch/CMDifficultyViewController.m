//
//  CMDifficultyViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/8/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMDifficultyViewController.h"

@interface CMDifficultyViewController ()

@end

@implementation CMDifficultyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    [self.view setExclusiveTouch:YES];

    self.sm = [CMSharedManager sharedManager];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UIButton *backButton = [CMSharedManager makeBackButton];
    [self.view addSubview:backButton];
    
    UILabel *headingLabel = [CMSharedManager makeHeadingLabelWithView:self.view WithText:@"Difficulties"];
    [self.view addSubview:headingLabel];

    
    for(int i = DIFFICULTY_START+1; i < DIFFICULTY_END; i++) {
        UIButton *difficultyButton = [CMSharedManager makeMenuButtonAtLocation:(i-DIFFICULTY_START-1) AndView:self.view AndSelector:@selector(clickDifficultyButton:) AndTitle:[NSString stringWithFormat:@"%@: %i X %i", [CMSharedManager getDifficultyName:i], i, i]];
        difficultyButton.tag = i;
        [self.view addSubview:difficultyButton];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) clickDifficultyButton:(UIButton *) sender
{
    CMGameViewController *gameViewController = [[CMGameViewController alloc] init];
    gameViewController.difficulty = (int)sender.tag;
    [self.navigationController pushViewController:gameViewController animated:YES];
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
