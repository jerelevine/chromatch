//
//  CMDifficultyViewController.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/8/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMGameViewController.h"
#import "CMSharedManager.h"


@interface CMDifficultyViewController : UIViewController

@property (strong, nonatomic) CMSharedManager *sm;

@end
