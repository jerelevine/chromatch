//
//  CMPauseViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/13/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMPauseViewController.h"

@interface CMPauseViewController ()

@end

@implementation CMPauseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setExclusiveTouch:YES];

    self.sm = [CMSharedManager sharedManager];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    
	// Do any additional setup after loading the view.
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UILabel *headingLabel = [CMSharedManager makeHeadingLabelWithView:self.view WithText:@"Paused"];
    [self.view addSubview:headingLabel];

    [self.view addSubview:headingLabel];
    
    UIButton *restartButton = [CMSharedManager makeMenuButtonAtLocation:0 AndView:self.view AndSelector:@selector(clickRestartButton:) AndTitle:@"Restart"];
    [self.view addSubview:restartButton];
    
    UIButton *quitButton = [CMSharedManager makeMenuButtonAtLocation:1 AndView:self.view AndSelector:@selector(clickQuitButton:) AndTitle:@"Quit"];
    [self.view addSubview:quitButton];
    
    UIButton *backButton = [CMSharedManager makeBackButton];
    
    [self.view addSubview:backButton];
    
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) clickRestartButton:(UIButton*) sender {
    if(self.alert) {
        [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:@"Restarting!" message:[NSString stringWithFormat:@"Your score will not be saved. Are you sure that you want to restart?"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [self.alert show];
}

-(void) clickQuitButton:(UIButton*) sender {
    if(self.alert) {
        [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:@"Quitting!" message:[NSString stringWithFormat:@"Your score will not be saved. Are you sure that you want to quit?"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [self.alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   if ([alertView.title isEqualToString:@"Restarting!"]) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            UINavigationController *navController = self.navigationController;
            
            // Pop this controller and replace with another
            [navController popViewControllerAnimated:NO];
            [navController popViewControllerAnimated:NO];
            CMGameViewController *gameViewController = [[CMGameViewController alloc] init];
            gameViewController.difficulty = self.difficulty;
            [navController pushViewController:gameViewController animated:YES];
        }
    }
    else if ([alertView.title isEqualToString:@"Quitting!"]) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            UINavigationController *navController = self.navigationController;
            
            // Pop this controller and replace with another
            // Goofy fix for a bug that will not allow a simple double pop
            [navController popViewControllerAnimated:NO];
            [navController popViewControllerAnimated:NO];
            [navController popViewControllerAnimated:NO];
            CMDifficultyViewController *difficultyViewController = [[CMDifficultyViewController alloc] init];
            [navController pushViewController:difficultyViewController animated:NO];
            [navController pushViewController:[[CMPauseViewController alloc] init] animated:NO];
            [navController popViewControllerAnimated:YES];
            
            /* Should work but is not in iOS 8
             [navController popViewControllerAnimated:NO];
             [navController popViewControllerAnimated:YES];
            */
        }
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
