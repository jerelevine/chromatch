//
//  CMOptionViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/10/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMOptionViewController.h"

@interface CMOptionViewController ()

@end

@implementation CMOptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setExclusiveTouch:YES];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    self.sm = [CMSharedManager sharedManager];
    
	// Do any additional setup after loading the view.
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    //[self.view setBackgroundColor:self.sm.infoTextColor];

    
    UIButton *backButton = [CMSharedManager makeBackButton];
    [self.view addSubview:backButton];
    
    UILabel *headingLabel = [CMSharedManager makeHeadingLabelWithView:self.view WithText:@"Options"];
    [self.view addSubview:headingLabel];
    
    
    
    UIButton *themeButton = [CMSharedManager makeMenuButtonAtLocation:0 AndView:self.view AndSelector:@selector(clickThemeButton:) AndTitle:@"Change Theme"];
    [self.view addSubview:themeButton];
    
    UIButton *shareButton = [CMSharedManager makeMenuButtonAtLocation:1 AndView:self.view AndSelector:@selector(clickShareButton:) AndTitle:@"Share"];
    [self.view addSubview:shareButton];


}

-(void) clickShareButton:(UIButton *) sender {
    [CMSharedManager shareToSocialMedia:self];
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) clickThemeButton:(UIButton *)sender {
    int themeNum = [self.sm themeNumber];
    int newThemeNum = (themeNum == 0 ? 1: 0);
    [self.sm setThemeNumber:newThemeNum];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:self.sm.themeNumber] forKey:@"THEME_NUMBER"];
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);

    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
