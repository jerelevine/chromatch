//
//  CMViewController.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "CMButton.h"
#import "CMGame.h"
#import "CMPauseViewController.h"
#import "CMSharedManager.h"
#import "GCHelper.h"



@interface CMGameViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *buttonArray;
@property (strong, nonatomic) CMGame *game;
@property (strong, nonatomic) UISegmentedControl *clickTypeButton;
@property (strong, nonatomic) UIButton *mainButton;
@property (weak, nonatomic) NSTimer *timer;
@property (strong, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) UILabel *pointLabel;
@property (nonatomic) double timeCount;
@property (nonatomic) int pointCount;
@property (nonatomic) BOOL isLabelDisplayed;

@property (strong, nonatomic) CMButton *firstChoice;
@property (strong, nonatomic) CMButton *secondChoice;
@property (nonatomic) int gameState;

@property (strong, nonatomic) UIView *buttonSubview;
@property (nonatomic) int difficulty;

@property (nonatomic) BOOL isPaused;
@property (nonatomic) int streakCount;

@property (strong, nonatomic) NSTimer *displayTimer;

@property (strong, nonatomic) UIAlertView *alert;



@property (strong, nonatomic) CMSharedManager *sm;

@end
