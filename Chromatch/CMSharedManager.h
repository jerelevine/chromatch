//
//  CMSharedManager.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/10/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMSharedManager : NSObject

@property (nonatomic, readonly) int BLOCK_MIN;
@property (nonatomic, readonly) int BLOCK_MAX;
@property (nonatomic, readonly) double TOP_GAP_BOARD;
@property (nonatomic, readonly) double TOP_GAP_LABEL;
@property (nonatomic, readonly) double TOP_GAP_INFO;
@property (nonatomic, readonly) double TOP_GAP_HIGH_SCORES;
@property (nonatomic, readonly) double SIDE_GAP;
@property (nonatomic, readonly) double BUTTON_GAP;
@property (nonatomic, readonly) double DESCRIPTION_GAP_FACTOR;


@property (nonatomic, readonly) double BLOCK_BORDER_WIDTH_NORMAL;
@property (nonatomic, readonly) double BLOCK_BORDER_WIDTH_HIGHLIGHTED;
@property (nonatomic, readonly) double BUTTON_BORDER_WIDTH;
@property (nonatomic, readonly) double CORNER_RADIUS;

@property (nonatomic, readonly) int TIMER_START;
@property (nonatomic, readonly) int TIMER_WARNING;
@property (nonatomic, readonly) double TIMER_TICK_INCREMENT;

@property (nonatomic, readonly) double STREAK_COUNT_MAX;
@property (nonatomic, readonly) double STREAK_COUNT_MULTIPLIER;

@property (nonatomic, readonly) double DISPLAY_TIME_WRONG;
@property (nonatomic, readonly) double DISPLAY_TIME_RESET;
@property (nonatomic, readonly) double DISPLAY_TIME_CORRECT;

@property (nonatomic, readonly) double MENU_WIDTH;
@property (nonatomic, readonly) double MENU_HEIGHT;

@property (nonatomic, readonly) double TITLE_WIDTH;
@property (nonatomic, readonly) double TITLE_HEIGHT;

@property (nonatomic, readonly) double LABEL_WIDTH;
@property (nonatomic, readonly) double LABEL_HEIGHT;

@property (nonatomic, readonly) double HEADING_WIDTH;
@property (nonatomic, readonly) double HEADING_HEIGHT;

@property (nonatomic, readonly) double AUXILIARY_WIDTH;
@property (nonatomic, readonly) double AUXILIARY_HEIGHT;

@property (nonatomic, readonly) double CYCLE_BUTTON_WIDTH;
@property (nonatomic, readonly) double CYCLE_BUTTON_HEIGHT;


@property (nonatomic, readonly) double TITLE_FONT_SIZE;
@property (nonatomic, readonly) double HEADING_FONT_SIZE;
@property (nonatomic, readonly) double MENU_FONT_SIZE;
@property (nonatomic, readonly) double LABEL_FONT_SIZE;
@property (nonatomic, readonly) double CYCLE_BUTTON_FONT_SIZE;
@property (nonatomic, readonly) double DESCRIPTION_FONT_SIZE;



@property (nonatomic, readonly) NSString *APP_TITLE;
@property (nonatomic, readonly) NSString *INSTRUCTIONS_TEXT;

@property (nonatomic, readonly) NSString *IMAGE_NAME_IN_PROGRESS;
@property (nonatomic, readonly) NSString *IMAGE_NAME_CORRECT;
@property (nonatomic, readonly) NSString *IMAGE_NAME_WRONG;

@property (nonatomic, readonly) NSString *IMAGE_NAME_BACKGROUND_GAME;
@property (nonatomic, readonly) NSString *IMAGE_NAME_BACKGROUND_MENU;

@property (strong, nonatomic, readonly) UIColor *headingTextColor;

@property (strong, nonatomic, readonly) UIColor *menuButtonTextColorNormal;
@property (strong, nonatomic, readonly) UIColor *menuButtonTextColorHighlighted;
@property (strong, nonatomic, readonly) UIColor *menuButtonBackgroundColor;


@property (strong, nonatomic, readonly) UIColor *blockButtonBorderColorNormal;
@property (strong, nonatomic, readonly) UIColor *blockButtonBorderColorHighlighted;
@property (strong, nonatomic, readonly) UIColor *blockButtonBorderColorHighlightedAlternative;

@property (strong, nonatomic, readonly) UIColor *infoTextColor;

@property (strong, nonatomic, readonly) UIColor *labelTextColor;
@property (strong, nonatomic, readonly) UIColor *warningLabelTextColor;


@property (nonatomic) int themeNumber;

@property (nonatomic) BOOL areAdsOff;
@property (nonatomic, readonly) BOOL DEBUG_CAN_DISPLAY_ADS;

@property (strong, nonatomic, readonly) NSArray *ACHIEVEMENT_POINTS_VALUES;
@property (strong, nonatomic, readonly) NSArray *ACHIEVEMENT_ROUNDS_VALUES;



typedef enum
{
    DIFFICULTY_START = 3,
    DIFFICULTY_EASY,
    DIFFICULTY_MEDIUM,
    DIFFICULTY_HARD,
    DIFFICULTY_VERY_HARD,
    DIFFICULTY_END
} DIFFICULTY_LEVEL;


+(instancetype) sharedManager;
+(UIButton *) makeBackButton;
+(UIButton *) makeMenuButtonWithX:(double) x AndY:(double) y AndSelector:(SEL)selector AndTitle:(NSString *)title;
+(UIButton *) makeMenuButtonAtLocation:(double)location AndView:(UIView *)view AndSelector:(SEL)selector AndTitle:(NSString *)title;

+(NSString *) getDifficultyName:(int) difficulty;
+(NSString *) getDifficultyNameWithUnderscore:(int) difficulty;
+(void) shareToSocialMedia:(UIViewController *) vc;
+(void) shareToSocialMedia:(UIViewController *) vc WithScore:(int) score;
+ (UILabel *) makeHeadingLabelWithView:(UIView *)view WithText:(NSString *)text;
@end
