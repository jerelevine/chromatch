//
//  CMHighScoresViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 6/11/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMHighScoresViewController.h"

@interface CMHighScoresViewController ()

@end

@implementation CMHighScoresViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setExclusiveTouch:YES];

    self.canDisplayBannerAds = !self.sm.areAdsOff && self.sm.DEBUG_CAN_DISPLAY_ADS;

    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    self.sm = [CMSharedManager sharedManager];
    
	// Do any additional setup after loading the view.
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];

    UIButton *backButton = [CMSharedManager makeBackButton];
    [self.view addSubview:backButton];
    
    UILabel *headingLabel = [CMSharedManager makeHeadingLabelWithView:self.view WithText:@"High Scores"];
    [self.view addSubview:headingLabel];
    
    NSString *text = [[NSMutableString alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    for(int i = DIFFICULTY_START+1; i <= DIFFICULTY_END; i++) {
        NSString *key = (i == DIFFICULTY_END)? @"LAST_SCORE":[NSString stringWithFormat:@"HIGH_SCORE_%i", i];
        NSString *difficultyString = [CMSharedManager getDifficultyName:i];
        int score = [[defaults objectForKey:key] intValue];
        NSString *scoreString = [NSString stringWithFormat:@"%i", score];
        text = [NSString stringWithFormat:@"%@:", (i == DIFFICULTY_END ? @"Last Game": difficultyString)];
        UILabel *instructionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-self.sm.HEADING_WIDTH/2, self.sm.TOP_GAP_HIGH_SCORES + (self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP)*((i-DIFFICULTY_START)), self.sm.HEADING_WIDTH, self.sm.HEADING_HEIGHT)];
        [instructionsLabel setTextColor:self.sm.infoTextColor];
        instructionsLabel.text = text;
        instructionsLabel.textAlignment = NSTextAlignmentCenter;
        [instructionsLabel setFont:[UIFont boldSystemFontOfSize:self.sm.MENU_FONT_SIZE]];
        [self.view addSubview:instructionsLabel];
        text = (i == DIFFICULTY_END) ? [NSString stringWithFormat:@"%@ - %@", [CMSharedManager getDifficultyName:[[defaults objectForKey:@"LAST_DIFFICULTY"] intValue]],scoreString] :[NSString stringWithFormat:@"%@", scoreString];
        instructionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-self.sm.HEADING_WIDTH/2, self.sm.TOP_GAP_HIGH_SCORES+ (self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP)*((i-DIFFICULTY_START-1)+1*self.sm.DESCRIPTION_GAP_FACTOR), self.sm.HEADING_WIDTH, self.sm.HEADING_HEIGHT)];
        [instructionsLabel setTextColor:self.sm.infoTextColor];
        instructionsLabel.text = text;
        instructionsLabel.textAlignment = NSTextAlignmentCenter;
        [instructionsLabel setFont:[UIFont boldSystemFontOfSize:self.sm.DESCRIPTION_FONT_SIZE]];
        [self.view addSubview:instructionsLabel];
    }
    
    UIButton *clearScoreButton = [CMSharedManager makeMenuButtonAtLocation:DIFFICULTY_END-DIFFICULTY_START AndView:self.view AndSelector:@selector(clickClearScoresButton:) AndTitle:@"Clear Scores"];
    [self.view addSubview:clearScoreButton];

    
    /*
    NSMutableString *text = [[NSMutableString alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    for(int i = DIFFICULTY_EASY; i < DIFFICULTY_END; i++) {
        NSString *key = [NSString stringWithFormat:@"HIGH_SCORE_%i", i];
        NSString *difficultyString = [CMSharedManager getDifficultyName:i];
        int score = [[defaults objectForKey:key] intValue];
        NSString *scoreString = [NSString stringWithFormat:@"%i", score];
        [text appendFormat:@"%@\n%@\n\n", difficultyString, scoreString];
    }
    
    NSString *keyDifficulty = @"LAST_DIFFICULTY";
    NSString *difficultyString = [CMSharedManager getDifficultyName:[[defaults objectForKey:keyDifficulty] intValue]];
    NSString *keyScore = @"LAST_SCORE";
    int score = [[defaults objectForKey:keyScore] intValue];
    NSString *scoreString = [NSString stringWithFormat:@"%i", score];
    [text appendFormat:@"Last Game\n%@ - %@", difficultyString, scoreString];
    

    
    UILabel *instructionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-self.sm.HEADING_WIDTH/2, self.sm.TOP_GAP_INFO*2, self.sm.HEADING_WIDTH, 400-(self.sm.TOP_GAP_INFO))];
    [instructionsLabel setTextColor:self.sm.infoTextColor];
    instructionsLabel.text = text;
    instructionsLabel.textAlignment = NSTextAlignmentCenter;
    [instructionsLabel setFont:[UIFont boldSystemFontOfSize:self.sm.MENU_FONT_SIZE]];
    instructionsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    instructionsLabel.numberOfLines = 0;
    [self.view addSubview:instructionsLabel];
    */
    
    
    
}

-(void) clickBackButton:(UIButton*) sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) clickClearScoresButton:(UIButton*) sender {
    if(self.alert) {
        [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:@"Clearing Scores!" message:[NSString stringWithFormat:@"Are you sure you want to clear your high scores?"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [self.alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([alertView.title isEqualToString:@"Clearing Scores!"]) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            [self clearScores];
            UINavigationController *navController = self.navigationController;
            
            // Pop this controller and replace with another
            [navController popViewControllerAnimated:NO];
            CMHighScoresViewController *highScoresViewController = [[CMHighScoresViewController alloc] init];
            [navController pushViewController:highScoresViewController animated:YES];
        }
    }
}

-(void) clearScores {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    for(int i = DIFFICULTY_START+1; i < DIFFICULTY_END; i++) {
        NSString *key = [NSString stringWithFormat:@"HIGH_SCORE_%i", i];
        [defaults setObject:[NSNumber numberWithInt:0] forKey:key];
    }
    [defaults setObject:[NSNumber numberWithInt:0] forKey:@"LAST_SCORE"];
    [defaults setObject:[CMSharedManager getDifficultyName:DIFFICULTY_END] forKey:@"LAST_DIFFICULTY"];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"didFailToReceiveAdWithError");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
