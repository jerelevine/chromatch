//
//  CMGameViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/7/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMMainViewController.h"

@interface CMMainViewController ()

@end

@implementation CMMainViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSLog(@"Nib");
        [[GCHelper defaultHelper] authenticateLocalUserOnViewController:self setCallbackObject:self withPauseSelector:@selector(pauseGame:)];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    self.sm = [CMSharedManager sharedManager];


    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_MENU]];
    [self.view addSubview:background];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-self.sm.TITLE_WIDTH/2, self.sm.TOP_GAP_INFO-self.sm.BUTTON_GAP, self.sm.TITLE_WIDTH, self.sm.TITLE_HEIGHT/2)];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:self.sm.TITLE_FONT_SIZE]];
    [titleLabel setText:self.sm.APP_TITLE];
    [titleLabel setTextColor:self.sm.headingTextColor];
    [self.view addSubview:titleLabel];
    
    UIButton *startButton = [CMSharedManager makeMenuButtonWithX:self.view.frame.size.width/2-self.sm.MENU_WIDTH/2 AndY:self.sm.TOP_GAP_INFO+(self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP) AndSelector:@selector(clickStart:) AndTitle:@"Start Game"];
    [self.view addSubview:startButton];
    
    UIButton *optionButton = [CMSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-self.sm.MENU_WIDTH/2 AndY: self.sm.TOP_GAP_INFO+(self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP)*2 AndSelector:@selector(clickOption:) AndTitle:@"Options"];
    [self.view addSubview:optionButton];
    
    UIButton *highScoresButton = [CMSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-self.sm.MENU_WIDTH/2 AndY: self.sm.TOP_GAP_INFO+(self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP)*3 AndSelector:@selector(clickHighScores:) AndTitle:@"High Scores"];
    [self.view addSubview:highScoresButton];
    
    UIButton *instructionsButton = [CMSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-self.sm.MENU_WIDTH/2 AndY: self.sm.TOP_GAP_INFO+(self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP)*4 AndSelector:@selector(clickInstructions:) AndTitle:@"Instructions"];
    [self.view addSubview:instructionsButton];
    
    UIButton *leaderboardButton = [CMSharedManager makeMenuButtonWithX: self.view.frame.size.width/2-self.sm.MENU_WIDTH/2 AndY: self.sm.TOP_GAP_INFO+(self.sm.MENU_HEIGHT+self.sm.BUTTON_GAP)*5 AndSelector:@selector(clickLeaderboard:) AndTitle:@"Leaderboard"];
    if([[GCHelper defaultHelper] gameCenterAvailable])
        [self.view addSubview:leaderboardButton];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) clickStart:(UIButton *)sender {
    CMDifficultyViewController *gameViewController = [[CMDifficultyViewController alloc] init];
    [self.navigationController pushViewController:gameViewController animated:YES];
}

- (void) clickOption:(UIButton *)sender {
    CMOptionViewController *optionViewController = [[CMOptionViewController alloc] init];
    [self.navigationController pushViewController:optionViewController animated:YES];
}

- (void) clickInstructions:(UIButton *)sender {
    CMInstructionsViewController *instructionsViewController = [[CMInstructionsViewController alloc] init];
    [self.navigationController pushViewController:instructionsViewController animated:YES];
}

-(void) clickHighScores:(UIButton *)sender {
    CMHighScoresViewController *highScoresViewController = [[CMHighScoresViewController alloc] init];
    [self.navigationController pushViewController:highScoresViewController animated:YES];
}

-(void) clickLeaderboard:(UIButton *)sender {
    [[GCHelper defaultHelper] showLeaderboardOnViewController:self];
}

-(void) pauseGame:(UIButton *)sender {
    
}



@end
