//
//  CMHighScoresViewController.h
//  Chromatch
//
//  Created by Jeremy Levine on 6/11/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSharedManager.h"
#import <iAd/iAd.h>


@interface CMHighScoresViewController : UIViewController

@property (strong, nonatomic) CMSharedManager *sm;
@property (strong, nonatomic) UIAlertView *alert;

@end
