//
//  CMPauseViewController.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/13/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSharedManager.h"
#import "CMGameViewController.h"
#import "CMDifficultyViewController.h"

@interface CMPauseViewController : UIViewController

@property (strong, nonatomic) CMSharedManager *sm;
@property (nonatomic) int difficulty;
@property (strong, nonatomic) UIAlertView *alert;

@end
