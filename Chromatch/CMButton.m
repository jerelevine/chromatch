//
//  CMButton.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import "CMButton.h"

@implementation CMButton



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}

- (instancetype) initWithLocation:(CGPoint)point andBlock:(CMBlock *)block andSize:(int)blockSize AndIsLabelDisplayed:(BOOL)isLabelDisplayed {
    self = [super initWithFrame:CGRectMake(point.x*blockSize, point.y*blockSize, blockSize, blockSize)];
    if (self) {
        self.sm = [CMSharedManager sharedManager];
        self.block = block;
        [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        [self.titleLabel setFont:[UIFont systemFontOfSize:ceil(blockSize/2.5f)]];
        [self setTitleColor: [UIColor blackColor] forState:UIControlStateNormal];
        CGFloat red, green, blue;
        [self.block.color getRed:&red green:&green blue:&blue alpha:nil];
        if((red*0.21f + green*0.71f + blue*0.07f) > 0.5)
            [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        else
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IsLabelDisplayed = isLabelDisplayed;
        self.backgroundColor = block.color;
        self.layer.borderColor = (self.sm.blockButtonBorderColorNormal).CGColor;
        self.layer.borderWidth = self.sm.BLOCK_BORDER_WIDTH_NORMAL;
        self.layer.cornerRadius = self.sm.CORNER_RADIUS;
        self.clipsToBounds = YES;
        self.chosen = NO;
        self.point = point;
    }
    return self;
}


-(void)setChosen:(BOOL)chosen {
    _chosen = chosen;
    if(!chosen) {
        self.layer.borderColor = (self.sm.blockButtonBorderColorNormal).CGColor;
        self.layer.borderWidth = self.sm.BLOCK_BORDER_WIDTH_NORMAL;
    }
    else {
        CGFloat red, green, blue;
        [self.block.color getRed:&red green:&green blue:&blue alpha:nil];
        if(red > 0.5 && red > green && red >= blue)
            self.layer.borderColor = (self.sm.blockButtonBorderColorHighlightedAlternative).CGColor;
        else
            self.layer.borderColor = (self.sm.blockButtonBorderColorHighlighted).CGColor;
        self.layer.borderWidth = self.sm.BLOCK_BORDER_WIDTH_HIGHLIGHTED;
    }

}

-(void) showMatching {
    if (self.block.hasMatch) {
        self.chosen = YES;
    }
    else {
        self.chosen = NO;
    }
}

-(void) setIsLabelDisplayed:(BOOL)isLabelDisplayed {
    _isLabelDisplayed = isLabelDisplayed;
    if(isLabelDisplayed)
        [self setTitle: self.block.label forState:UIControlStateNormal];
    else
        [self setTitle: nil forState:UIControlStateNormal];

}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
