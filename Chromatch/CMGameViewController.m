//
//  CMViewController.m
//  Chromatch
//
//  Created by Jeremy Levine on 2/4/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//


//
// Potential Power-ups: Swap, Shuffle, +5 seconds, 50% Time Penalty, +50% Points
//

#import "CMGameViewController.h"

@interface CMGameViewController ()

@end

@implementation CMGameViewController


-(void) viewDidAppear:(BOOL)animated {
    if(self.isPaused) self.timer = [NSTimer scheduledTimerWithTimeInterval:self.sm.TIMER_TICK_INCREMENT target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    self.isPaused = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(!self.isPaused && self.gameState != STATE_QUIT && self.gameState != STATE_OVER)[self pauseGame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setExclusiveTouch:YES];
   
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    self.sm = [CMSharedManager sharedManager];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(pauseGame)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    //Displays ads
    self.canDisplayBannerAds = (!self.sm.areAdsOff) && self.sm.DEBUG_CAN_DISPLAY_ADS;
  
	// Do any additional setup after loading the view, typically from a nib.
    [self setup];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setup
{
    
    self.isPaused = NO;
    self.isLabelDisplayed = NO;
    self.timeCount = self.sm.TIMER_START;
    
   
    self.buttonArray = [[NSMutableArray alloc] init];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_BACKGROUND_GAME]];
    [self.view addSubview:background];
    
    self.firstChoice = nil;
    self.secondChoice = nil;
    
    UILabel *headingLabel = [CMSharedManager makeHeadingLabelWithView:self.view WithText:self.sm.APP_TITLE];
    [headingLabel setFrame:CGRectMake(self.view.frame.size.width/2-self.sm.HEADING_WIDTH/2, self.sm.TOP_GAP_LABEL, self.sm.HEADING_WIDTH, self.sm.HEADING_HEIGHT)];
    [self.view addSubview:headingLabel];
    
    UIButton *backButton = [CMSharedManager makeBackButton];
    [self.view addSubview:backButton];
    
    UIButton *pauseButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-self.sm.SIDE_GAP-self.sm.AUXILIARY_WIDTH, self.sm.TOP_GAP_LABEL, self.sm.AUXILIARY_WIDTH, self.sm.AUXILIARY_HEIGHT)];
    [pauseButton setTitle: @"l l" forState:UIControlStateNormal];
    [pauseButton setTitleColor:self.sm.menuButtonTextColorNormal forState:UIControlStateNormal];
    [pauseButton setTitleColor:self.sm.menuButtonTextColorHighlighted forState:UIControlStateHighlighted];
    pauseButton.backgroundColor = self.sm.menuButtonBackgroundColor;
    pauseButton.layer.cornerRadius = self.sm.CORNER_RADIUS;
    [pauseButton.titleLabel setFont:[UIFont systemFontOfSize:self.sm.MENU_FONT_SIZE]];
    pauseButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    pauseButton.clipsToBounds = YES;
    [pauseButton setExclusiveTouch:YES];
    [pauseButton addTarget:self action:@selector(clickPauseButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:pauseButton];


    
    self.pointLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.sm.SIDE_GAP, self.sm.TOP_GAP_INFO, self.sm.LABEL_WIDTH, self.sm.LABEL_HEIGHT)];
    self.pointLabel.text = [self labelString:self.pointCount];
    self.pointLabel.textAlignment = NSTextAlignmentCenter;
    self.pointLabel.layer.backgroundColor = (self.sm.menuButtonBackgroundColor).CGColor;
    self.pointLabel.textColor = self.sm.labelTextColor;
    [self.pointLabel setFont:[UIFont systemFontOfSize:self.sm.LABEL_FONT_SIZE]];
    self.pointLabel.layer.cornerRadius = self.sm.CORNER_RADIUS;
    
    [self.view addSubview:self.pointLabel];

    
    self.timerLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-self.sm.SIDE_GAP-self.sm.LABEL_WIDTH, self.sm.TOP_GAP_INFO, self.sm.LABEL_WIDTH, self.sm.LABEL_HEIGHT)];
    self.timerLabel.text = [self labelString:self.timeCount];
    self.timerLabel.textAlignment = NSTextAlignmentCenter;
    self.timerLabel.layer.backgroundColor = (self.sm.menuButtonBackgroundColor).CGColor;
    self.timerLabel.textColor = self.sm.labelTextColor;
    [self.timerLabel setFont:[UIFont systemFontOfSize:self.sm.LABEL_FONT_SIZE]];
    self.timerLabel.layer.cornerRadius = self.sm.CORNER_RADIUS;

    [self.view addSubview:self.timerLabel];
    
    self.mainButton = [[UIButton alloc] init];
    self.mainButton.frame = CGRectMake(self.view.frame.size.width/2-self.sm.CYCLE_BUTTON_WIDTH/2, self.sm.TOP_GAP_INFO, self.sm.CYCLE_BUTTON_WIDTH, self.sm.CYCLE_BUTTON_WIDTH);
    self.mainButton.backgroundColor = self.sm.menuButtonBackgroundColor;
    self.mainButton.layer.cornerRadius = self.sm.CORNER_RADIUS;
    [self.mainButton.titleLabel setFont:[UIFont systemFontOfSize:self.sm.CYCLE_BUTTON_FONT_SIZE]];
    self.mainButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.mainButton addTarget:self action:@selector(clickMainButton:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:self.mainButton];
    
    self.gameState = STATE_PRE_START;
    self.pointCount = 0;
    self.streakCount = 0;

    if(self.alert) {
        [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:@"Get ready to select the two identical colors!" message:[NSString stringWithFormat:@"Click Ok to start"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [self.alert show];
}

-(void) newGame {
    if(!self.timer) self.timer = [NSTimer scheduledTimerWithTimeInterval:self.sm.TIMER_TICK_INCREMENT target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    self.gameState = STATE_IN_PROGRESS;
    
    [self.buttonArray removeAllObjects];
    self.game = [[CMGame alloc] initWithDifficulty:self.difficulty];
    int blockSize = fmin(self.view.frame.size.width/self.difficulty, self.sm.BLOCK_MAX);
    if(blockSize < self.sm.BLOCK_MIN) [NSException raise:@"Invalid Block size value" format:@"Block size of %d is invalid", blockSize];
    
    [self.buttonSubview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.buttonSubview removeFromSuperview];
    
    if(!self.buttonSubview)
        self.buttonSubview = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2.0f-(self.game.width*blockSize)/2.0f), self.sm.TOP_GAP_BOARD, self.game.width * blockSize, self.game.height * blockSize)];
    
    for(int i = 0; i < self.game.height; i++) {
        for (int j = 0; j < self.game.width; j++) {
            CMButton *button = [[CMButton alloc] initWithLocation: CGPointMake(j,i) andBlock:self.game.board[j][i] andSize:blockSize AndIsLabelDisplayed:self.isLabelDisplayed];
            [self.buttonArray addObject:button];
            [self.buttonSubview addSubview:button];
            [button addTarget:self action:@selector(clickBlockButton:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    [self.view addSubview:self.buttonSubview];
}

-(void) onTick:(NSTimer *)timer {
    self.timeCount -= self.sm.TIMER_TICK_INCREMENT;
    self.timerLabel.text = [self labelString:ceil(self.timeCount)];
    
    //Make timer red at 10 seconds and less
    if(self.timeCount <= self.sm.TIMER_WARNING ) {
        self.timerLabel.textColor = self.sm.warningLabelTextColor;
    }
    
    if(self.timeCount <= 0) {
        [self.timer invalidate];
        [self gameOver];
    }
}

-(NSString *) labelString:(int) num {
    if(num < 10) {
        return [NSString stringWithFormat:@"00%i",num];
    }
    else if(num < 100) {
        return [NSString stringWithFormat:@"0%i",num];
    }
    else if(num < 1000) {
        return [NSString stringWithFormat:@"%i",num];
    }
    else
        return @"---";
}


-(void) clickBlockButton:(CMButton *)sender
{
    if(self.gameState == STATE_RESET || self.gameState == STATE_WRONG) return;
    
    UIColor *color = sender.backgroundColor;
    CGFloat red, green, blue, alpha;
    [color getRed: &red green: &green blue: &blue alpha: &alpha];
    double x = sqrt(pow(red, 2) + pow(green, 2) + pow(blue,2));
    NSLog(@"Color: (%f, %f, %f) %@, %f", red, green, blue, sender.titleLabel.text, x);
    
    if(!self.firstChoice) {
        self.firstChoice = sender;
        sender.chosen = YES;
    }
    else if(sender.chosen) {
        self.firstChoice = nil;
        sender.chosen = NO;
    }
    else if(self.firstChoice && !sender.chosen && [[self.firstChoice.block color] isEqual:[sender.block color]]) {
        self.secondChoice = sender;
        [self gameCorrect];
    }
    else if(self.gameState != STATE_CORRECT){
        self.secondChoice = sender;
        [self gameWrong];
    }
}

-(void) clickMainButton:(UIButton *)sender {
    if(self.gameState == STATE_IN_PROGRESS) {
        [self gameReset];
    }
}

-(void) gameWrong {
    self.gameState = STATE_WRONG;
    self.firstChoice.chosen = YES;
    self.secondChoice.chosen = YES;
    self.pointCount--;
    self.streakCount = 0;
    self.displayTimer = [NSTimer scheduledTimerWithTimeInterval:self.sm.DISPLAY_TIME_WRONG target:self selector:@selector(setGameInProgress) userInfo:nil repeats:NO];
}

-(void) gameReset {
    for(CMButton *b in self.buttonArray) {
        [b showMatching];
    }
    self.gameState = STATE_RESET;
    if(self.firstChoice) self.firstChoice.chosen = NO;
    if(self.secondChoice) self.secondChoice.chosen = NO;
    self.firstChoice = nil;
    self.secondChoice = nil;
    self.displayTimer = [NSTimer scheduledTimerWithTimeInterval:self.sm.DISPLAY_TIME_RESET target:self selector:@selector(newGame) userInfo:nil repeats:NO];
}

-(void) gameCorrect {
    self.firstChoice.chosen = YES;
    self.secondChoice.chosen = YES;
    //double factor = self.sm.STREAK_COUNT_MULTIPLIER*(self.streakCount/self.sm.STREAK_COUNT_MAX) + 1.0f;
    double factor = ((pow(self.difficulty,2)/100.0)*2)*(self.streakCount/self.sm.STREAK_COUNT_MAX) + 1.0f;
    int points = round(floor(pow(self.difficulty,2)) * factor);
    NSLog(@"Streak: %i, Points: %i", self.streakCount, points);
    self.pointCount+= points;
    [self.firstChoice setTitle:[NSString stringWithFormat:@"+%i", (int)ceil(points/2.0f)] forState:UIControlStateNormal];
    [self.secondChoice setTitle:[NSString stringWithFormat:@"+%i", (int)floor(points/2.0f)] forState:UIControlStateNormal];
    self.streakCount++;
    self.gameState = STATE_CORRECT;
    self.firstChoice = nil;
    self.secondChoice = nil;
    self.displayTimer = [NSTimer scheduledTimerWithTimeInterval:self.sm.DISPLAY_TIME_CORRECT target:self selector:@selector(newGame) userInfo:nil repeats:NO];
}

-(void) gameOver {
    self.gameState = STATE_OVER;
    [self.displayTimer invalidate];
    [self setHighScore];
    [self incrementTotalRoundsPlayed];
    [self reportToGC];
    NSString *s = (self.pointCount == 1 ? @"" : @"s");
    if(self.alert) {
        [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:@"Time's Up!" message:[NSString stringWithFormat:@"You got %i point%@!", self.pointCount, s] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Share", nil];
    // optional - add more buttons:
    [self.alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
    NSString *viewTitle = alertView.title;

    if([viewTitle isEqualToString:@"Time's Up!"]) {
        [self.timer invalidate];
        if ([buttonTitle isEqualToString: @"Share"]) {
            [CMSharedManager shareToSocialMedia:self WithScore:self.pointCount];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if ([viewTitle isEqualToString:@"Get ready to select the two identical colors!"]) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            self.gameState = STATE_IN_PROGRESS;
            [self newGame];
        }
    }
    else if ([viewTitle isEqualToString:@"Quitting!"]) {
        if (buttonIndex == alertView.firstOtherButtonIndex && self.gameState != STATE_OVER) {
            [self.timer invalidate];
            self.gameState = STATE_QUIT;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void) motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    // Listens for the shake
    /*if(motion == UIEventSubtypeMotionShake)
    {
        self.isLabelDisplayed = !self.isLabelDisplayed;
        for(CMButton *b in self.buttonArray) {
            [b setIsLabelDisplayed:self.isLabelDisplayed];
        }
    }*/
}

-(void) setGameInProgress {
    self.firstChoice.chosen = NO;
    self.secondChoice.chosen = NO;
    self.firstChoice = nil;
    self.secondChoice = nil;
    self.gameState = STATE_IN_PROGRESS;
}


-(void) setGameState:(int)gameState {
    _gameState = gameState;
    if(gameState == STATE_RESET || gameState == STATE_WRONG) {
        [self.mainButton setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_WRONG] forState:UIControlStateNormal];
    }
    else if(gameState == STATE_CORRECT) {
        [self.mainButton setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_CORRECT] forState:UIControlStateNormal];
    }
    else {
        [self.mainButton setImage:[UIImage imageNamed:self.sm.IMAGE_NAME_IN_PROGRESS] forState:UIControlStateNormal];
    }
}

-(void) clickBackButton:(UIButton *)sender {
    if(self.alert) {
        [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:@"Quitting!" message:[NSString stringWithFormat:@"Your score will not be saved. Are you sure that you want to quit?"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    // optional - add more buttons:
    [self.alert show];
}

-(void) clickPauseButton:(UIButton *)sender {
    [self pauseGame];
}

-(void) setPointCount:(int)pointCount {
    _pointCount = pointCount;
    if(_pointCount < 0) _pointCount = 0;
    self.pointLabel.text = [self labelString:self.pointCount];
}

-(void) setStreakCount:(int)streakCount {
    _streakCount = streakCount;
    if(_streakCount > self.sm.STREAK_COUNT_MAX) _streakCount = self.sm.STREAK_COUNT_MAX;
}

-(void)pauseGame {
    if(!self.isPaused && self.gameState != STATE_PRE_START) {
        self.isPaused = YES;
        if(self.timer)[self.timer invalidate];
        if(self.displayTimer) {
            [self.displayTimer fire];
            [self.displayTimer invalidate];
        }
        if(self.alert) {
            [self.alert dismissWithClickedButtonIndex:self.alert.cancelButtonIndex animated:NO];
        }
        CMPauseViewController *pauseViewController = [[CMPauseViewController alloc] init];
        pauseViewController.difficulty = self.difficulty;
        [self.navigationController pushViewController:pauseViewController animated:YES];
    }
}

-(void) setHighScore {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *key = [NSString stringWithFormat:@"HIGH_SCORE_%i", self.difficulty];
    int highScore = [[defaults objectForKey:key] intValue];
    if(self.pointCount > highScore) {
        [defaults setObject:[NSNumber numberWithInt:self.pointCount] forKey:key];
    }
    int newScore = [[defaults objectForKey:key] intValue];
    NSLog(@"High Score = %i", newScore);
    
    
    [defaults setObject:[NSNumber numberWithInt:self.pointCount] forKey:@"LAST_SCORE"];
    [defaults setObject: [NSNumber numberWithInt:self.difficulty] forKey:@"LAST_DIFFICULTY"];

}

-(void) incrementTotalRoundsPlayed {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int totalRoundsPlayed = [[defaults objectForKey:@"TOTAL_ROUNDS_PLAYED"] intValue];
    totalRoundsPlayed++;
    [defaults setObject:[NSNumber numberWithInt:totalRoundsPlayed] forKey:@"TOTAL_ROUNDS_PLAYED"];
    for (int i = 0; i < [self.sm.ACHIEVEMENT_ROUNDS_VALUES count]; i++) {
        int rounds = [self.sm.ACHIEVEMENT_ROUNDS_VALUES[i] doubleValue];
        NSString *identifier = [NSString stringWithFormat:@"Achievement_%i_Rounds_Played", rounds];
        [[GCHelper defaultHelper] reportAchievementIdentifier:identifier percentComplete:MIN(100.0, 100*((double) totalRoundsPlayed) / ((double) rounds))];
    }
}

-(void) reportToGC {
    //Report high scores
    [[GCHelper defaultHelper]reportScore:self.pointCount forLeaderboardID:[NSString stringWithFormat:@"Chromatch_%@_Leaderboard", [CMSharedManager getDifficultyNameWithUnderscore:self.difficulty]]];

    //Report achievements
    for (int i = 0; i < [self.sm.ACHIEVEMENT_POINTS_VALUES count]; i++) {
        int value = [self.sm.ACHIEVEMENT_POINTS_VALUES[i] intValue];
        if(self.pointCount >= value) {
            NSString *identifier = [NSString stringWithFormat:@"Achievement_%i_Points_%@", value, [CMSharedManager getDifficultyNameWithUnderscore:self.difficulty]];
            [[GCHelper defaultHelper] reportAchievementIdentifier:identifier percentComplete:100.0];
        }
    }

}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"didFailToReceiveAdWithError");
}


@end
