//
//  CMOptionViewController.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/10/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSharedManager.h"


@interface CMOptionViewController : UIViewController

@property (strong, nonatomic) CMSharedManager *sm;

@end
