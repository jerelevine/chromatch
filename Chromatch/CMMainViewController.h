//
//  CMGameViewController.h
//  Chromatch
//
//  Created by Jeremy Levine on 2/7/14.
//  Copyright (c) 2014 Jeremy Levine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "CMDifficultyViewController.h"
#import "CMOptionViewController.h"
#import "CMInstructionsViewController.h"
#import "CMHighScoresViewController.h"
#import "GCHelper.h"

@interface CMMainViewController : UIViewController

@property (strong, nonatomic) CMSharedManager *sm;


@end
